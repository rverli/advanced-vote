#DockerFile for K8s
FROM openjdk:17-alpine
COPY target/*.jar /advanced-vote.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/advanced-vote.jar"]