package com.vote.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vote.controller.v1.AgendaController;
import com.vote.model.dto.AgendaDto;
import com.vote.service.AgendaService;

@ExtendWith(MockitoExtension.class)
class AgendaControllerTest {

	private static final String ENDPOINT_URL = "/v1/agenda";

	@InjectMocks
	private AgendaController controller;

	@Mock
	private AgendaService service;

	private MockMvc mockMvc;

	@BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	@Test
	@DisplayName("Test save Agenda - Success")
	void save() throws Exception {
		
		when(service.create(any())).thenReturn(this.createAgendaDto());

		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_URL).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(this.createAgendaDto())))
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.jsonPath("$.nmAgenda", Is.is("Agenda1")));

		Mockito.verify(service, Mockito.times(1)).create(any());
		Mockito.verifyNoMoreInteractions(service);
	}
	
	@Test
    @DisplayName("Test update agenda - Success")
    void update() throws Exception {

    	when(service.update(anyInt(), any())).thenReturn(this.createAgendaDto());
        
        mockMvc.perform(MockMvcRequestBuilders.put(ENDPOINT_URL + "/{id}", "2").contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(this.createAgendaDto())))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nmAgenda", Is.is("Agenda1")));

        Mockito.verify(service, Mockito.times(1)).update(anyInt(), any());
        Mockito.verifyNoMoreInteractions(service);
    }

	@Test
    @DisplayName("Test delete agenda - Success")
    void delete() throws Exception {

		doNothing().when(service).delete(anyInt());
		
        mockMvc.perform(MockMvcRequestBuilders.delete(ENDPOINT_URL + "/{id}", "2"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        Mockito.verify(service, Mockito.times(1)).delete(anyInt());
        Mockito.verifyNoMoreInteractions(service);
    }
	
	private AgendaDto createAgendaDto() {
	    return AgendaDto.builder().nmAgenda("Agenda1").subject("Subject").build();
	  }
}
