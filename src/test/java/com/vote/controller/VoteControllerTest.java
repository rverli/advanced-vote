package com.vote.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vote.controller.v1.VoteController;
import com.vote.model.dto.CountVotes;
import com.vote.model.dto.SessionDto;
import com.vote.model.dto.SummarizeVotesDto;
import com.vote.model.dto.VoteDto;
import com.vote.service.SessionService;
import com.vote.service.VoteService;

@ExtendWith(MockitoExtension.class)
class VoteControllerTest {

	private static final String ENDPOINT_URL = "/v1/vote";

	@InjectMocks
	private VoteController controller;

	@Mock
	private VoteService service;

	@Mock
	private SessionService sessionService;
	
	private MockMvc mockMvc;

	@BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	@Test
	@DisplayName("Test save Vote - Success")
	void save() throws Exception {
		
		doNothing().when(service).produceVote(any(), any());
		when(sessionService.findSessionByName(anyString())).thenReturn(createSessionDto());

		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_URL).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(this.createVoteDto())))
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

		Mockito.verify(service, Mockito.times(1)).produceVote(any(), any());
		Mockito.verify(sessionService, Mockito.times(1)).findSessionByName(anyString());
		Mockito.verifyNoMoreInteractions(service);
	}
	
	@Test
	@DisplayName("Test save Vote - Success")
	void count() throws Exception {
		
		when(service.countVotes(any())).thenReturn(SummarizeVotesDto.builder().result(List.of(CountVotes.builder().qtd(20L).vote("Sim").build())).build());
		when(sessionService.findSessionById(anyInt())).thenReturn(createSessionDto());

		mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_URL + "/count/{sessionId}", "1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

		Mockito.verify(service, Mockito.times(1)).countVotes(any());
		Mockito.verify(sessionService, Mockito.times(1)).findSessionById(anyInt());
		Mockito.verifyNoMoreInteractions(service);
	}
	
	private VoteDto createVoteDto() {
	    return VoteDto.builder().id(2).vote("Sim").nmSession("session1").build();
	  }
	
	private SessionDto createSessionDto() {
		return SessionDto.builder().nmSession("session1").ttl(360).id(1).anonymous(true)
				.dtStart(LocalDateTime.now().plusMonths(1)).build();
	}
}
