package com.vote.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vote.controller.v1.SessionController;
import com.vote.model.dto.SessionDto;
import com.vote.service.impl.SessionServiceImpl;

@ExtendWith(MockitoExtension.class)
class SessionControllerTest {

	private static final String ENDPOINT_URL = "/v1/session";

	@InjectMocks
	private SessionController controller;

	@Mock
	private SessionServiceImpl service;

	private MockMvc mockMvc;

	@BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	@Test
	@DisplayName("Test save session - Success")
	void save() throws Exception {
		
		when(service.create(any())).thenReturn(this.createSessionDto());

		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_URL).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(this.createSessionDto())))
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.jsonPath("$.nmSession", Is.is("session1")));
	}
	
	@Test
    @DisplayName("Test update session - Success")
    void update() throws Exception {

    	when(service.update(anyInt(), any())).thenReturn(this.createSessionDto());
        
        mockMvc.perform(MockMvcRequestBuilders.put(ENDPOINT_URL + "/{id}", "2").contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(this.createSessionDto())))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nmSession", Is.is("session1")));
    }
	
	@Test
    @DisplayName("Test get session - Success")
    void get() throws Exception {

    	when(service.findSessionById(anyInt())).thenReturn(this.createSessionDto());
        
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_URL + "/{id}", "2"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nmSession", Is.is("session1")));
    }

	@Test
    @DisplayName("Test get session - Success")
    void getByName() throws Exception {

    	when(service.findSessionByName(anyString())).thenReturn(this.createSessionDto());
        
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_URL + "/name/{name}", "session1"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nmSession", Is.is("session1")));
    }
	
	@Test
    @DisplayName("Test delete session - Success")
    void delete() throws Exception {

		doNothing().when(service).delete(anyInt());
		
        mockMvc.perform(MockMvcRequestBuilders.delete(ENDPOINT_URL + "/{id}", "2"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }
	
	private SessionDto createSessionDto() {
		return SessionDto.builder().nmSession("session1").ttl(360).id(1).anonymous(true)
				.dtStart(LocalDateTime.now().plusMonths(1)).build();
	}
}
