package com.vote.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vote.infrastructure.client.UserNoAuthClient;
import com.vote.infrastructure.jms.producer.TopicResultProducer;
import com.vote.infrastructure.jms.producer.TopicVoteProducer;
import com.vote.model.converter.SessionMapper;
import com.vote.model.converter.UserMapper;
import com.vote.model.converter.VoteMapper;
import com.vote.model.dto.CountVotes;
import com.vote.model.dto.SessionDto;
import com.vote.model.dto.SummarizeVotesDto;
import com.vote.model.dto.UserDto;
import com.vote.model.dto.VoteDto;
import com.vote.model.entity.Session;
import com.vote.model.entity.User;
import com.vote.model.entity.Vote;
import com.vote.model.exception.CpfInvalidException;
import com.vote.model.exception.SessionNotAnonymousException;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.model.exception.SessionPeriodException;
import com.vote.model.exception.UserNotFoundException;
import com.vote.model.exception.UserUnableToVoteException;
import com.vote.model.exception.VoteException;
import com.vote.repository.VoteRepository;
import com.vote.service.impl.SessionServiceImpl;
import com.vote.service.impl.VoteServiceImpl;

@ExtendWith(MockitoExtension.class)
class VoteServiceTest {

	@InjectMocks
	private VoteServiceImpl service;
	@Mock
	private SessionServiceImpl sessionService;
	@Mock
	private UserService userService;
	@Mock
	private UserNoAuthClient userClient;
	@Mock
	private VoteRepository repository;
	@Mock
	private TopicResultProducer resultProducer;
	@Mock
	private TopicVoteProducer voteProducer;
	@Mock
	private SessionMapper sessionMapper;
	@Mock
	private VoteMapper voteMapper;
	@Mock
	private UserMapper userMapper;

	@Test
	void releaseResultSessionTest() throws VoteException {
		
		doNothing().when(resultProducer).send(any());
		when(repository.summarizeVotes(anyInt())).thenReturn(
				List.of(createCountVotes()));
		
		service.releaseResultSession(createSessionDto());

		verify(resultProducer, timeout(1)).send(any());
	}
	
	@Test
	void countVotesTest() {

		when(repository.summarizeVotes(createSessionDto().getId())).thenReturn(List.of(createCountVotes()));

		SummarizeVotesDto countVotes = service.countVotes(createSessionDto());

		assertNotNull(countVotes);
		assertEquals(createSessionDto().getNmSession(), countVotes.getSession().getNmSession());
		assertEquals(10, countVotes.getResult().get(0).getQtd());
	}

	@Test
	void createVoteTest() throws VoteException, UserNotFoundException {

		when(userService.findUserByCpf(anyString())).thenReturn(createUserDto());
		when(repository.save(any())).thenReturn(createVote());
		when(sessionMapper.toEntity(any())).thenReturn(createSession());
		when(voteMapper.toEntity(any())).thenReturn(createVote());
		when(voteMapper.toDto(any())).thenReturn(createVoteDto());
		when(userMapper.toEntity(any())).thenReturn(createUser());

		VoteDto vote = service.create(createVoteDto(), null);

		assertNotNull(vote);
		assertEquals("session1", vote.getNmSession());
		assertEquals("nao", vote.getVote());
	}

	@Test
	void createVoteException() throws VoteException, UserNotFoundException {

		when(userService.findUserByCpf(anyString())).thenReturn(createUserDto());
		Vote createVote = createVote();
		createVote.setVote(null);
		when(voteMapper.toEntity(any())).thenReturn(createVote);
		
		Throwable exception = assertThrows(VoteException.class, () -> service.create(createVoteDto(), null));

		assertEquals("Error to compute vote!", exception.getMessage());
	}
	
	@Test
	void produceVote() throws UserNotFoundException, SessionPeriodException, SessionNotAnonymousException, VoteException {

		when(userService.findUserByCpf(anyString())).thenReturn(createUserDto());
		when(repository.findBySessionNmSessionAndUserCpf(anyString(), anyString())).thenReturn(null);
		doNothing().when(voteProducer).send(any());
		
		service.produceVote(createSessionDto(), createVoteDto());

		verify(userService, times(1)).findUserByCpf(anyString());
		verify(repository, times(1)).findBySessionNmSessionAndUserCpf(anyString(), anyString());
		verify(voteProducer, times(1)).send(any());
	}
	
	@Test
	void produceVoteException() throws UserNotFoundException, SessionPeriodException, SessionNotAnonymousException, VoteException {

		when(userService.findUserByCpf(anyString())).thenReturn(createUserDto());
		when(repository.findBySessionNmSessionAndUserCpf(anyString(), anyString())).thenReturn(createVote());
		
		Throwable exception = assertThrows(VoteException.class, () -> service.produceVote(createSessionDto(), createVoteDto()));

		assertEquals("User has already voted in session!", exception.getMessage());
	}
	
	@Test
	void produceVoteUserNotFoundException() throws UserNotFoundException, SessionPeriodException, SessionNotAnonymousException, VoteException {

		when(userService.findUserByCpf(anyString())).thenThrow(UserNotFoundException.class);
		when(repository.findBySessionNmSessionAndUserCpf(anyString(), anyString())).thenReturn(null);
		when(userService.create(any())).thenReturn(createUserDto());
		when(userMapper.toEntity(any())).thenReturn(createUser());
		doNothing().when(voteProducer).send(any());

		service.produceVote(createSessionDto(), createVoteDto());

		verify(userService, times(1)).findUserByCpf(anyString());
		verify(repository, times(1)).findBySessionNmSessionAndUserCpf(anyString(), anyString());
	}
	
	@Test
	void produceVoteSessionNotAnonymousException() throws UserNotFoundException, SessionPeriodException, SessionNotAnonymousException, VoteException {

		VoteDto vote = createVoteDto();
		vote.setUser(null);
		SessionDto session = createSessionDto();
		session.setAnonymous(false);
		
		Throwable exception = assertThrows(SessionNotAnonymousException.class, () -> service.produceVote(session, vote));

		assertEquals("Session is not anonymous! Please send the user", exception.getMessage());
	}
	
	@Test
	void produceVotePeriodExceptionTest() throws SessionNotFoundException, UserNotFoundException, SessionPeriodException,
			CpfInvalidException, UserUnableToVoteException, VoteException, JSONException, SessionNotAnonymousException {

		SessionDto createSessionDto = createSessionDto();
		createSessionDto.setDtStart(LocalDateTime.of(2019, 1, 1, 1, 1));

		Throwable exception = assertThrows(SessionPeriodException.class, () -> service.produceVote(createSessionDto, createVoteDto()));

		assertEquals("Vote not allowed, Session is not open!", exception.getMessage());
	}
	
	private SessionDto createSessionDto() {
		return SessionDto.builder().nmSession("session1").ttl(360).id(1).anonymous(false).dtStart(LocalDateTime.now())
				.build();
	}

	private Session createSession() {
		return Session.builder().nmSession("session1").ttl(360).id(1).anonymous(true).dtStart(LocalDateTime.now())
				.build();
	}

	private CountVotes createCountVotes() {
		return CountVotes.builder().qtd(10).vote("Sim").build();
	}

	private UserDto createUserDto() {
		return UserDto.builder().nmUser("user1").id(1).cpf("08512824790").build();
	}

	private User createUser() {
		return User.builder().nmUser("user1").id(1).cpf("08512824790").build();
	}

	private Vote createVote() {
		return Vote.builder().id(1).vote("nao").session(createSession()).user(createUser()).build();
	}

	private VoteDto createVoteDto() {
		return VoteDto.builder().id(1).vote("nao").nmSession(createSession().getNmSession()).user(createUserDto())
				.build();
	}

}
