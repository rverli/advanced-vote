package com.vote.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vote.model.converter.AgendaMapper;
import com.vote.model.dto.AgendaDto;
import com.vote.model.entity.Agenda;
import com.vote.repository.AgendaRepository;
import com.vote.service.impl.AgendaServiceImpl;

@ExtendWith(MockitoExtension.class)
class AgendaServiceTest {

	@InjectMocks
	private AgendaServiceImpl service;
	@Mock
	private AgendaRepository repository;
	@Mock
	private AgendaMapper agendaMapper;

	@Test
	void deleteTest() {
		doNothing().when(repository).deleteById(anyInt());
		service.delete(anyInt());
		verify(repository, times(1)).deleteById(anyInt());
	}

	@Test
	void updateTest() {

		when(repository.save(any())).thenReturn(createAgenda());

		when(agendaMapper.toEntity(any())).thenReturn(createAgenda());
		when(agendaMapper.toDto(any())).thenReturn(createAgendaDto());
		
		AgendaDto update = service.update(1, createAgendaDto());

		assertNotNull(update);
		assertEquals("Agenda1", update.getNmAgenda());
		verify(repository, times(1)).save(any());
	}

	@Test
	void saveTest() {

		when(repository.save(any())).thenReturn(createAgenda());

		when(agendaMapper.toEntity(any())).thenReturn(createAgenda());
		when(agendaMapper.toDto(any())).thenReturn(createAgendaDto());
		
		AgendaDto update = service.create(createAgendaDto());

		assertNotNull(update);
		assertEquals("Agenda1", update.getNmAgenda());
		verify(repository, times(1)).save(any());
	}
	
	private Agenda createAgenda() {
		return Agenda.builder().id(1).nmAgenda("Agenda1").subject("Subject").build();
	}
	private AgendaDto createAgendaDto() {
		return AgendaDto.builder().nmAgenda("Agenda1").subject("Subject").build();
	}
}
