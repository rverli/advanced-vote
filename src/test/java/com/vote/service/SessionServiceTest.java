package com.vote.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vote.infrastructure.scheduler.VoteResultScheduler;
import com.vote.model.converter.SessionMapper;
import com.vote.model.dto.SessionDto;
import com.vote.model.entity.Session;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.repository.SessionRepository;
import com.vote.service.impl.SessionServiceImpl;

@ExtendWith(MockitoExtension.class)
class SessionServiceTest {

	@InjectMocks
	private SessionServiceImpl service;
	@Mock
	private SessionRepository repository;
	@Mock
	private VoteResultScheduler resultScheduler;
	@Mock
	private SessionMapper sessionMapper;

	@Test
	void deleteTest() {
		doNothing().when(repository).deleteById(anyInt());
		service.delete(anyInt());
		verify(repository, times(1)).deleteById(anyInt());
	}

	@Test
	void updateTest() {

		when(repository.save(any())).thenReturn(createSession());
		when(sessionMapper.toEntity(any())).thenReturn(createSession());
		when(sessionMapper.toDto(any())).thenReturn(createSessionDto());
		
		SessionDto update = service.update(1, createSessionDto());

		assertNotNull(update);
		assertEquals(1, update.getId());
	}

	@Test
	void saveTest() {

		when(repository.save(any())).thenReturn(createSession());
		when(sessionMapper.toEntity(any())).thenReturn(createSession());
		when(sessionMapper.toDto(any())).thenReturn(createSessionDto());

		SessionDto sve = service.create(createSessionDto());

		assertNotNull(sve);
		assertEquals(1, sve.getId());
	}

	@Test
	void findSessionByNameTest() throws SessionNotFoundException {

		when(repository.findByNmSession(anyString())).thenReturn(Optional.of(createSession()));
		when(sessionMapper.toDto(any())).thenReturn(createSessionDto());

		SessionDto sessionReturn = service.findSessionByName(anyString());

		assertNotNull(sessionReturn);
		assertEquals(1, sessionReturn.getId());
	}

	@Test
	void findSessionByIdTest() throws SessionNotFoundException {

		when(repository.findById(anyInt())).thenReturn(Optional.of(createSession()));
		when(sessionMapper.toDto(any())).thenReturn(createSessionDto());

		SessionDto sessionReturn = service.findSessionById(anyInt());

		assertNotNull(sessionReturn);
		assertEquals(1, sessionReturn.getId());
	}
	
	@Test
	void findSessionByIdExceptionTest() {
		SessionNotFoundException ex = assertThrows(SessionNotFoundException.class, () -> {
			service.findSessionById(anyInt());
		});
		assertNotNull(ex);
	}

	@Test
	void findSessionByNameTestExceptionTest() {
		SessionNotFoundException ex = assertThrows(SessionNotFoundException.class, () -> {
			service.findSessionByName(anyString());
		});
		assertNotNull(ex);
	}
	
	@Test
	void findAllByDtStartAfterDateTest() throws SessionNotFoundException {

		Session session = createSession();

		LocalDateTime time = LocalDateTime.of(2021, 1, 1, 1, 1);

		when(repository.findAllByDtStartAfterDate(time)).thenReturn(List.of(session));

		List<Session> listReturn = service.findAllByDtStartAfterNow(time);

		assertNotNull(listReturn);
		assertEquals(1, listReturn.size());
	}

	private SessionDto createSessionDto() {
		return SessionDto.builder().nmSession("session1").ttl(360).id(1).anonymous(true)
				.dtStart(LocalDateTime.now().plusMonths(1)).build();
	}

	private Session createSession() {
		return Session.builder().nmSession("session1").ttl(360).id(1).anonymous(true)
				.dtStart(LocalDateTime.now().plusMonths(1)).build();
	}
}
