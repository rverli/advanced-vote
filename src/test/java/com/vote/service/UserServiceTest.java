package com.vote.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vote.model.converter.UserMapper;
import com.vote.model.dto.UserDto;
import com.vote.model.entity.User;
import com.vote.model.exception.UserNotFoundException;
import com.vote.repository.UserRepository;
import com.vote.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

	@InjectMocks
	private UserServiceImpl service;
	@Mock
	private UserRepository repository;
	@Mock
	private UserMapper userMapper;
	
	@Test
	void save() {

		User user = createUser();

		when(repository.save(any())).thenReturn(user);
		when(userMapper.toDto(any())).thenReturn(createUserDto());

		UserDto sve = service.create(user);

		assertNotNull(sve);
		assertEquals(1, sve.getId());
	}

	@Test
	void findSessionByCPF() throws UserNotFoundException {

		when(repository.findByCpf(anyString())).thenReturn(Optional.of(createUser()));
		when(userMapper.toDto(any())).thenReturn(createUserDto());

		UserDto userReturn = service.findUserByCpf(anyString());

		assertNotNull(userReturn);
		assertEquals(1, userReturn.getId());
	}
	
	@Test
	void findUserByCpfException() {
		UserNotFoundException ex = assertThrows(UserNotFoundException.class, () -> {
			service.findUserByCpf(anyString());
		});
		assertNotNull(ex);
	}

	private User createUser() {
		return User.builder().nmUser("user1").id(1).cpf("08512824790").build();
	}
	
	private UserDto createUserDto() {
		return UserDto.builder().nmUser("user1").id(1).cpf("08512824790").build();
	}
}
