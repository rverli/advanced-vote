package com.vote.util;

import java.text.Normalizer;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtil {

  public static String removerAcentos(String str) {
    return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
  }

  public static String removerPontuacao(String str) {
    return str.replaceAll("\\p{Punct}", "");
  }
}
