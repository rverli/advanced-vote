package com.vote.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vote.model.entity.Session;

public interface SessionRepository extends JpaRepository<Session, Integer> {

  Optional<Session> findByNmSession(String name);

  @Query("select s from Session s where s.dtStart >= :date")
  List<Session> findAllByDtStartAfterDate(@Param("date") LocalDateTime creationDateTime);
}
