package com.vote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vote.model.entity.Agenda;

public interface AgendaRepository extends JpaRepository<Agenda, Integer> {
}
