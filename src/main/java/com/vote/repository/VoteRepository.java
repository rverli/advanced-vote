package com.vote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vote.model.dto.CountVotes;
import com.vote.model.entity.Vote;

public interface VoteRepository extends JpaRepository<Vote, Integer> {

  Vote findBySessionNmSessionAndUserCpf(String nmSession, String cpf);

  @Query(
      value = "select new com.vote.model.dto.CountVotes(v.vote, count(1)) from Vote v where v.session.id = ?1 group by v.vote")
  List<CountVotes> summarizeVotes(Integer sessionId);
}
