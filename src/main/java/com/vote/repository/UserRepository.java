package com.vote.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vote.model.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
  Optional<User> findByCpf(String cpf);
}
