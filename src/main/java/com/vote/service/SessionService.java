package com.vote.service;

import java.time.LocalDateTime;
import java.util.List;

import com.vote.model.dto.SessionDto;
import com.vote.model.entity.Session;
import com.vote.model.exception.SessionNotFoundException;

public interface SessionService {

  /**
   * Remove Session by Id
   * 
   * @param id
   */
  void delete(int id);

  /**
   * Update Session
   * @param id 
   * 
   * @param toUpdate
   * @return
   */
  SessionDto update(int id, SessionDto toUpdate);

  /**
   * Create Session
   * 
   * @param toCreate
   * @return
   */
  SessionDto create(SessionDto toCreate);

  /**
   * Find Session by Name
   * 
   * @param name
   * @return
   * @throws SessionNotFoundException
   */
  SessionDto findSessionByName(String name) throws SessionNotFoundException;

  /**
   * Find Session by Id
   * 
   * @param id
   * @return
   * @throws SessionNotFoundException
   */
  SessionDto findSessionById(int id) throws SessionNotFoundException;

  /**
   * Find all sessions that have the start date after the param date
   * 
   * @param dateTime
   * @return
   */
  List<Session> findAllByDtStartAfterNow(LocalDateTime dateTime);
}
