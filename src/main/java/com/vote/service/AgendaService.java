package com.vote.service;

import com.vote.model.dto.AgendaDto;

public interface AgendaService {

	/**
	   * Remove Agenda by Id
	   * 
	   * @param id
	   */
	void delete(int id);

	/**
	   * Update Agenda
	   * @param id 
	   * 
	   * @param toUpdate
	   * @return
	   */
	AgendaDto update(Integer id, AgendaDto toUpdate);

	/**
	   * Create Agenda
	   * 
	   * @param toCreate
	   * @return
	   */
	AgendaDto create(AgendaDto toCreate);

}
