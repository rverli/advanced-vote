package com.vote.service.impl;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.vote.infrastructure.client.UserNoAuthClient;
import com.vote.infrastructure.jms.producer.TopicResultProducer;
import com.vote.infrastructure.jms.producer.TopicVoteProducer;
import com.vote.model.converter.SessionMapper;
import com.vote.model.converter.UserMapper;
import com.vote.model.converter.VoteMapper;
import com.vote.model.dto.CountVotes;
import com.vote.model.dto.SessionDto;
import com.vote.model.dto.SummarizeVotesDto;
import com.vote.model.dto.UserDto;
import com.vote.model.dto.VoteDto;
import com.vote.model.entity.User;
import com.vote.model.entity.Vote;
import com.vote.model.enumerated.CheckUserAbleToVoteEnum;
import com.vote.model.exception.CpfInvalidException;
import com.vote.model.exception.SessionNotAnonymousException;
import com.vote.model.exception.SessionPeriodException;
import com.vote.model.exception.UserNotFoundException;
import com.vote.model.exception.UserUnableToVoteException;
import com.vote.model.exception.VoteException;
import com.vote.repository.VoteRepository;
import com.vote.service.UserService;
import com.vote.service.VoteService;
import com.vote.util.StringUtil;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class VoteServiceImpl implements VoteService {

	private final VoteRepository repository;
	private final UserService userService;
	private final UserNoAuthClient userClient;
	private final TopicResultProducer resultProducer;
	private final TopicVoteProducer voteProducer;
	
	private final SessionMapper sessionMapper;
	private final VoteMapper voteMapper;
	private final UserMapper userMapper;

	/**
	 * {@inheritDoc}
	 * @throws VoteException 
	 * @throws JsonProcessingException 
	 */
	@Override
	public void releaseResultSession(SessionDto sessionDto) throws VoteException {
		SummarizeVotesDto countVotes = this.countVotes(sessionDto);
		try {
			resultProducer.send(new ObjectMapper().writeValueAsString(countVotes));
		} catch (JsonProcessingException e) {
			throw new VoteException(e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SummarizeVotesDto countVotes(SessionDto sessionDto) {

		List<CountVotes> count = repository.summarizeVotes(sessionDto.getId());

		return SummarizeVotesDto.builder()
				.session(sessionDto)
				.result(count)
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void produceVote(SessionDto session, VoteDto voteDto) 
			throws SessionPeriodException, SessionNotAnonymousException, VoteException {
		this.validateVote(session, voteDto);
		voteProducer.send(new Gson().toJson(voteDto));
	}
	
	@Override
	public VoteDto create(VoteDto voteDto, SessionDto session) throws VoteException {

		/* Check if its a secret session */
		UserDto userDto = this.checkUser(voteDto, session);

		/* Save vote if there is vote(yes or no) */
		Vote vote = this.saveVote(voteMapper.toEntity(voteDto), session, userDto);

		return voteMapper.toDto(vote);
	}

	private void validateVote(SessionDto session, VoteDto voteDto) 
			throws SessionPeriodException, VoteException, SessionNotAnonymousException {
		
		/* Check if the session is open and the user can vote */
		this.checkOpenSession(session);

		/**
		 * API de validação de CPF não esta funcionando
		 */
		/* Check if user can vote */
		//this.checkUserAbleToVote(voteDto.getUser());

		this.checkAnonymousSession(session, voteDto.getUser());

		/* Check if its a secret session */
		UserDto userDto = this.checkUser(voteDto, session);

		/* Check if User has already voted in session */
		this.checkUserVote(session.getNmSession(), userDto);
	}
	
	private void checkAnonymousSession(SessionDto session, UserDto user) throws SessionNotAnonymousException {

		if (!session.isAnonymous() && Objects.isNull(user)) {
			throw new SessionNotAnonymousException("Session is not anonymous! Please send the user");
		}
	}

	private void checkUserVote(String nmSession, UserDto userDto) throws VoteException {

		if (Objects.isNull(userDto)) return;

		Vote vote = repository.findBySessionNmSessionAndUserCpf(nmSession, userDto.getCpf());

		if (Objects.nonNull(vote)) {
			throw new VoteException("User has already voted in session!");
		}
	}

	private Vote saveVote(Vote vote, SessionDto sessionDto, UserDto userDto) throws VoteException {

		if (Objects.nonNull(vote.getVote())) {
			vote.setSession(sessionMapper.toEntity(sessionDto));
			vote.setUser(!Objects.isNull(userDto) ? userMapper.toEntity(userDto) : null);
			vote.setVote(StringUtil.removerAcentos(vote.getVote()).toUpperCase());

			vote = repository.save(vote);

			log.info("Vote session {} saved!", vote.getSession().getNmSession());
		} else {
			throw new VoteException("Error to compute vote!");
		}
		return vote;
	}

	private UserDto checkUser(VoteDto dto, SessionDto sessionDto) {

		if (Objects.isNull(dto) || Objects.isNull(dto.getUser())) return null;

		try {
			return userService.findUserByCpf(StringUtil.removerPontuacao(dto.getUser().getCpf()));
		} catch (UserNotFoundException e) {
			if (!sessionDto.isAnonymous()) {
				dto.getUser().setCpf(StringUtil.removerPontuacao(dto.getUser().getCpf()));
				User user = userMapper.toEntity(dto.getUser());
				return userService.create(user);
			}
		}
		return null;
	}

	/**
	 * API de validação de CPF não disponível
	 */
	@Deprecated
	private void checkUserAbleToVote(UserDto req) throws CpfInvalidException, JSONException, UserUnableToVoteException {

		if (Objects.isNull(req)) return;

		String result = null;

		try {
			result = userClient.getUser(StringUtil.removerPontuacao(req.getCpf()));
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CpfInvalidException();
		}

		if (!Objects.isNull(result) && !result.isEmpty()) {
			String status = new JSONObject(result).getString("status");

			if (status.equalsIgnoreCase(CheckUserAbleToVoteEnum.UNABLE_TO_VOTE.name())) {
				log.info("User {} not allowed to vote!", req.getNmUser());
				throw new UserUnableToVoteException("User is not allowed to vote!");
			}
		}
	}

	private void checkOpenSession(SessionDto sessionDto) throws SessionPeriodException {

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime start = sessionDto.getDtStart();
		LocalDateTime end = sessionDto.getDtStart().plusMinutes(sessionDto.getTtl());

		if (now.isAfter(start.minus(1, ChronoUnit.MINUTES)) && now.isBefore(end)) {
			log.info("Session {} open!", sessionDto.getNmSession());
		} else {
			log.info("Session {} is not open or ended!", sessionDto.getNmSession());
			throw new SessionPeriodException("Vote not allowed, Session is not open!");
		}
	}
}
