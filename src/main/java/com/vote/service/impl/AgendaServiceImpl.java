package com.vote.service.impl;

import org.springframework.stereotype.Service;

import com.vote.model.converter.AgendaMapper;
import com.vote.model.dto.AgendaDto;
import com.vote.model.entity.Agenda;
import com.vote.repository.AgendaRepository;
import com.vote.service.AgendaService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AgendaServiceImpl implements AgendaService {

	private final AgendaRepository repository;
	private final AgendaMapper agendaMapper;

	/** {@inheritDoc} */
	@Override
	public void delete(int id) {
		repository.deleteById(id);
		log.info("Agenda {} deleted!", id);
	}

	/** {@inheritDoc} */
	@Override
	public AgendaDto update(Integer id, AgendaDto toUpdate) {

		Agenda agenda = agendaMapper.toEntity(toUpdate);
		agenda.setId(id);

		Agenda agendaDB = repository.save(agenda);
		log.info("Agenda {} updated!", agenda.getNmAgenda());

		return agendaMapper.toDto(agendaDB);
	}

	/** {@inheritDoc} */
	@Override
	public AgendaDto create(AgendaDto toCreate) {

		Agenda entity = agendaMapper.toEntity(toCreate);
		
		Agenda agenda = repository.save(entity);
		log.info("Agenda {} saved!", agenda.getNmAgenda());

		return agendaMapper.toDto(agenda);
	}
}
