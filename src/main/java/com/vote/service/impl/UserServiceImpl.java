package com.vote.service.impl;

import org.springframework.stereotype.Service;

import com.vote.model.converter.UserMapper;
import com.vote.model.dto.UserDto;
import com.vote.model.entity.User;
import com.vote.model.exception.UserNotFoundException;
import com.vote.repository.UserRepository;
import com.vote.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository repository;
  private final UserMapper userMapper;

  /** {@inheritDoc} */
  @Override
  public UserDto findUserByCpf(String cpf) throws UserNotFoundException {
    
	  User user = repository.findByCpf(cpf).orElseThrow(UserNotFoundException::new);
	  log.info("Found {} by name!", cpf);
    
	  return userMapper.toDto(user);
  }

  /** {@inheritDoc} */
  @Override
  public UserDto create(User toCreate) {
	  
    User user = repository.save(toCreate);
    log.info("User {} saved!", user.getNmUser());
    
    return userMapper.toDto(user);
  }
}
