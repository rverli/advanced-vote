package com.vote.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.vote.infrastructure.scheduler.VoteResultScheduler;
import com.vote.model.converter.SessionMapper;
import com.vote.model.dto.SessionDto;
import com.vote.model.entity.Session;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.repository.SessionRepository;
import com.vote.service.SessionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class SessionServiceImpl implements SessionService {

	private final SessionRepository repository;
	private final VoteResultScheduler resultScheduler;
	private final SessionMapper sessionMapper;

	/** {@inheritDoc} */
	@Override
	public void delete(int id) {
		repository.deleteById(id);
		log.info("Session {} deleted!", id);
	}

	/** {@inheritDoc} */
	@Override
	public SessionDto update(int id, SessionDto toUpdate) {

		Session session = sessionMapper.toEntity(toUpdate);
		session.setId(id);

		Session sessionDB = repository.save(session);
		log.info("Session {} updated!", session.getNmSession());

		return sessionMapper.toDto(sessionDB);
	}

	/** {@inheritDoc} */
	@Override
	public SessionDto create(SessionDto toCreate) {

		Session entity = sessionMapper.toEntity(toCreate);
		
		Session session = repository.save(entity);
		log.info("Session {} saved!", session.getNmSession());

		/* Agendar envio do resultado ao final da sessão */
		resultScheduler.schedulingResults(session);

		return sessionMapper.toDto(session);
	}

	/** {@inheritDoc} */
	@Override
	public SessionDto findSessionByName(String name) throws SessionNotFoundException {

		Optional<Session> session = Optional
				.ofNullable(repository.findByNmSession(name).orElseThrow(SessionNotFoundException::new));
		log.info("Found {} by name!", name);

		return session.isPresent() ? sessionMapper.toDto(session.get()) : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SessionDto findSessionById(int id) throws SessionNotFoundException {

		Optional<Session> session = Optional
				.ofNullable(repository.findById(id).orElseThrow(SessionNotFoundException::new));
		log.info("Found session with id - {}!", id);

		return session.isPresent() ? sessionMapper.toDto(session.get()) : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Session> findAllByDtStartAfterNow(LocalDateTime dateTime) {
		return repository.findAllByDtStartAfterDate(dateTime);
	}
}
