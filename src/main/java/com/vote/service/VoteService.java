package com.vote.service;

import com.vote.model.dto.SessionDto;
import com.vote.model.dto.SummarizeVotesDto;
import com.vote.model.dto.VoteDto;
import com.vote.model.exception.SessionNotAnonymousException;
import com.vote.model.exception.SessionPeriodException;
import com.vote.model.exception.VoteException;

public interface VoteService {

  /**
   * Receive a vote and treat then following the rules
   * @param voteDto
   * @param sessionDto
   * @return
   * @throws VoteException
   */
  VoteDto create(VoteDto voteDto, SessionDto sessionDto) throws VoteException;

  /**
   * Send the result of the session to the queue that will be consumed by another service
   * 
   * @param session
   * @throws VoteException 
   */
  void releaseResultSession(SessionDto session) throws VoteException;

  /**
   * Count all votes of the session
   * 
   * @param sessionId
   * @return
   */
  SummarizeVotesDto countVotes(SessionDto sessionDto);

  /**
   * Validate and Produce Vote
   * 
   * @param session
   * @param voteDto
   * @throws SessionPeriodException
   * @throws SessionNotAnonymousException
   * @throws VoteException
   */
  void produceVote(SessionDto session, VoteDto voteDto)
		throws SessionPeriodException, SessionNotAnonymousException, VoteException;
}
