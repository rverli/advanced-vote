package com.vote.service;

import com.vote.model.dto.UserDto;
import com.vote.model.entity.User;
import com.vote.model.exception.UserNotFoundException;

public interface UserService {

  /**
   * Create User
   * 
   * @param toCreate
   * @return
   */
  UserDto create(User toCreate);

  /**
   * Find User by CPF
   * 
   * @param cpf
   * @return
   * @throws UserNotFoundException
   */
  UserDto findUserByCpf(String cpf) throws UserNotFoundException;
}
