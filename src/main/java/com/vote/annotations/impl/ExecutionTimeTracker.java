package com.vote.annotations.impl;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class ExecutionTimeTracker {

    @Around("@annotation(com.vote.annotations.TrackerTime)")
    public Object trackTime(ProceedingJoinPoint pjp) throws Throwable {
    	
        Object obj = null;
        LocalDateTime start = this.logInit(pjp);

        try {
            obj = pjp.proceed();
        } catch (Throwable e) {
            this.logEnd(pjp, start);
            throw e;
        }

        this.logEnd(pjp, start);
        return obj;
    }

    private LocalDateTime logInit(ProceedingJoinPoint pjp) {
    	
        LocalDateTime start = LocalDateTime.now();
        
        String params = null;
        try {
            params = String.join(", ", getParametersNameWithValue(pjp));
        } catch (Exception e) {
        	log.error("Its was not possible to concatenate request params.");
        }
        
        log.debug("BEGIN CALL --> {} - {}", getMethodName(pjp.getSignature()), 
        		StringUtils.isEmpty(params) ? "[without params]" : "[" + params + "]");
        
        return start;
    }

    private void logEnd(ProceedingJoinPoint pjp, LocalDateTime start) {
    	
    	LocalDateTime end = LocalDateTime.now();
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        
        log.debug("END CALL --> {} -> Request: Start: {} | End: {} - Duration: {} ms",
                getMethodName(pjp.getSignature()),
                start.format(formatter),
                end.format(formatter),
                Duration.between(start, end).toMillis());
    }

    private String getMethodName(Signature signature) {

        try {
            String clazz = signature.getDeclaringType().getName().substring(
                    signature.getDeclaringType().getName().lastIndexOf(".") + 1
            );

            return clazz.concat("::").concat(signature.getName());
        } catch (StringIndexOutOfBoundsException e) {
            return signature.getName();
        }
    }

    private List<String> getParametersNameWithValue(ProceedingJoinPoint pjp) {
    	
        MethodSignature methodSig = (MethodSignature) pjp.getSignature();
        Object[] args = pjp.getArgs();
        String[] parametersName = methodSig.getParameterNames();

        List<String> response = new ArrayList<>();

        int idx = 0;
        for (String pn : parametersName) {
            try {
                response.add(pn + ": " + args[idx]);
            } catch (ArrayIndexOutOfBoundsException e) {
                response.add(pn + ": null");
            }
            idx++;

        }
        return response;
    }
}