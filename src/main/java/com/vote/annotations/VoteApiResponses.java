package com.vote.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ApiResponses({@ApiResponse(
    responseCode = "200",
    description = "Sucesso"
), @ApiResponse(
	responseCode = "201",
	description = "Nos métodos POST, indica que a entidade foi criada com sucesso"
), @ApiResponse(
	responseCode = "401",
	description = "Usuário não autenticado"
), @ApiResponse(
	responseCode = "403",
	description = "Acesso negado"
), @ApiResponse(
	responseCode = "400",
	description = "Ocorreu um erro negocial (por exemplo, de validação)"
), @ApiResponse(
	responseCode = "404",
	description = "Entidade inexistente"
), @ApiResponse(
	responseCode = "500",
	description = "Ocorreu um erro inesperado. O erro foi logado e será investigado pela equipe técnica"
)})
public @interface VoteApiResponses {
}
