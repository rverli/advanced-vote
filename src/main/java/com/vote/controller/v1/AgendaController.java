package com.vote.controller.v1;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vote.annotations.TrackerTime;
import com.vote.annotations.VoteApiResponses;
import com.vote.model.dto.AgendaDto;
import com.vote.service.AgendaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "Agenda")
@Tag(name = "Agenda")
@VoteApiResponses
@RestController
@RequestMapping("/v1/agenda")
@RequiredArgsConstructor
public class AgendaController {

  private final AgendaService service;

  /**
   * * Create a agenda.
   * <p>
   * All requests are saved in a database
   * </p>
   * @param vote
   */
  @TrackerTime
  @ApiOperation(value = "Create Agenda")
  @PostMapping
  public AgendaDto create(@RequestBody @Valid AgendaDto agenda) {
	log.info("creating agenda");
	return service.create(agenda);
  }
  
  /**
	 * Update Agenda.
	 * <p>
	 * All requests are saved in a database and can be updated in the service
	 * </p>
	 * 
	 * @param agendaDto
	 * @return AgendaDto
	 */
	@TrackerTime
	@ApiOperation(value = "Update agenda request")
	@PutMapping("/{id}")
	public AgendaDto update(@PathVariable Integer id, @RequestBody @Valid AgendaDto sessionDto) {
		return service.update(id, sessionDto);
	}

	/**
	 * Delete Agenda by id.
	 * <p>
	 * All requests are saved in a database and can be deleted in the service
	 * </p>
	 * 
	 * @param id
	 */
	@TrackerTime
	@ApiOperation(value = "Delete agenda requests by id")
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Integer id) {
		service.delete(id);
	}
}
