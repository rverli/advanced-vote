package com.vote.controller.v1;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vote.annotations.TrackerTime;
import com.vote.annotations.VoteApiResponses;
import com.vote.model.dto.SessionDto;
import com.vote.model.dto.SummarizeVotesDto;
import com.vote.model.dto.VoteDto;
import com.vote.model.exception.SessionNotAnonymousException;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.model.exception.SessionPeriodException;
import com.vote.model.exception.VoteException;
import com.vote.service.SessionService;
import com.vote.service.VoteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "Vote")
@Tag(name = "Vote")
@VoteApiResponses
@RestController
@RequestMapping("/v1/vote")
@RequiredArgsConstructor
public class VoteController {

  private final VoteService voteService;
  private final SessionService sessionService;

  /**
   * * Create a Vote.
   * <p>
   * All requests are saved in a database
   * </p>
   * @param vote
   * @throws SessionNotFoundException
   * @throws SessionPeriodException
   * @throws SessionNotAnonymousException
   * @throws VoteException
   */
  @TrackerTime
  @ApiOperation(value = "Save vote")
  @PostMapping
  public void create(@RequestBody @Valid VoteDto vote) 
		  throws SessionNotFoundException, SessionPeriodException, 
		  SessionNotAnonymousException, VoteException  {
	log.info("Sending vote to topic");
    SessionDto session = sessionService.findSessionByName(vote.getNmSession());
    voteService.produceVote(session, vote);
  }

  /**
   * Count Votes by session.
   * <p>
   * All requests are saved in a database
   * </p>
   * 
   * @param sessionId
   * @return
   * @throws SessionNotFoundException 
   */
  @TrackerTime
  @ApiOperation(value = "Count votes")
  @GetMapping("/count/{sessionId}")
  public SummarizeVotesDto count(@PathVariable int sessionId) throws SessionNotFoundException {
    log.info("Counting votes");
    SessionDto sessionDto = sessionService.findSessionById(sessionId);
    return voteService.countVotes(sessionDto);
  }
}
