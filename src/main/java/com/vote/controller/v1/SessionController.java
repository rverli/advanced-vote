package com.vote.controller.v1;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vote.annotations.TrackerTime;
import com.vote.annotations.VoteApiResponses;
import com.vote.model.dto.SessionDto;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.service.SessionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@Api(tags = "Session")
@Tag(name = "Session")
@VoteApiResponses
@RestController
@RequestMapping("/v1/session")
@RequiredArgsConstructor
public class SessionController {

	private final SessionService service;

	/**
	 * Get Session by id.
	 * <p>
	 * All requests are saved in a database and can be requested in the service
	 * </p>
	 * 
	 * @param id
	 * @return SessionDto
	 * @throws SessionNotFoundException
	 */
	@TrackerTime
	@ApiOperation(value = "Return session requests by id")
	@GetMapping("/{id}")
	public SessionDto get(@PathVariable Integer id) throws SessionNotFoundException {
		return service.findSessionById(id);
	}

	/**
	 * Get Session by name.
	 * <p>
	 * All requests are saved in a database and can be requested in the service
	 * </p>
	 * 
	 * @param id
	 * @return SessionDto
	 * @throws SessionNotFoundException
	 */
	@TrackerTime
	@ApiOperation(value = "Return session requests name id")
	@GetMapping("/name/{name}")
	public SessionDto get(@PathVariable String name) throws SessionNotFoundException {
		return service.findSessionByName(name);
	}

	/**
	 * Create a Session.
	 * <p>
	 * All requests are saved in a database
	 * </p>
	 * 
	 * @param sessionDto
	 * @return SessionDto
	 */
	@TrackerTime
	@ApiOperation(value = "Create session")
	@PostMapping
	public SessionDto create(@RequestBody @Valid SessionDto sessionDto) {
		return service.create(sessionDto);
	}

	/**
	 * Update Session.
	 * <p>
	 * All requests are saved in a database and can be updated in the service
	 * </p>
	 * 
	 * @param sessionDto
	 * @return SessionDto
	 */
	@TrackerTime
	@ApiOperation(value = "Update session request")
	@PutMapping("/{id}")
	public SessionDto update(@PathVariable int id, @RequestBody @Valid SessionDto sessionDto) {
		return service.update(id, sessionDto);
	}

	/**
	 * Delete Session by id.
	 * <p>
	 * All requests are saved in a database and can be deleted in the service
	 * </p>
	 * 
	 * @param id
	 */
	@TrackerTime
	@ApiOperation(value = "Delete session requests by id")
	@DeleteMapping("/{id}")
	public void delete(@PathVariable int id) {
		service.delete(id);
	}
}
