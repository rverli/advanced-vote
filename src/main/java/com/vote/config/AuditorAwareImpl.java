package com.vote.config;

import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<String> {
	
    @Override
    public Optional<String> getCurrentAuditor() {
    	
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	
    	if (Objects.isNull(authentication)) {
    		return Optional.empty();
    	}
    	
        return Optional.of(authentication.getName());
    }
}
