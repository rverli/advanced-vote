package com.vote.config;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {

  @Bean
  Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
    		.apiInfo(apiInfo())
    		.select()
    		.apis(RequestHandlerSelectors.basePackage("com.vote.controller"))
    		.paths(PathSelectors.any())
    		.build().directModelSubstitute(Timestamp.class, String.class)
    		.useDefaultResponseMessages(false)
    		.securityContexts(securityContexts());
  }
  
  private ApiInfo apiInfo() {
      return new ApiInfo(
              "Advanced Vote",
              "Advanced Vote",
              "1",
              "Terms of service",
              new Contact("XPTO", "URL", "EMAIL"),
              "License of API",
              "API license URL",
              Collections.emptyList());
  }
  
  private List<SecurityContext> securityContexts() {
      List<SecurityContext> contexts = new ArrayList<>();
      contexts.add(SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/v1/*")).build());

      return contexts;
  }

  private List<SecurityReference> defaultAuth() {
      AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
      AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
      authorizationScopes[0] = authorizationScope;

      return Arrays.asList(new SecurityReference("JWT", authorizationScopes));
  }
}
