package com.vote.infrastructure.scheduler;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import com.vote.model.converter.SessionMapper;
import com.vote.model.entity.Session;
import com.vote.model.exception.VoteException;
import com.vote.service.VoteService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class VoteResultScheduler {

  private final VoteService voteService;
  private final TaskScheduler executor;
  private final SessionMapper sessionMapper;
  
  public void schedulingResults(List<Session> sessionFutureTime) {

    sessionFutureTime.stream()
        .forEach(s -> this.scheduling(
	        			s.getDtStart().plusMinutes(s.getTtl()), 
	        			s));
  }

  public void schedulingResults(Session session) {

    LocalDateTime endSession = session.getDtStart().plusMinutes(session.getTtl());

    this.scheduling(endSession, session);
  }

  public void scheduling(LocalDateTime resultTime, Session session) {

    log.info("Scheduling result of session {} for {}", session.getNmSession(), resultTime);
    Runnable task = () -> {
		try {
			voteService.releaseResultSession(sessionMapper.toDto(session));
		} catch (VoteException e) {
			log.error(e.getMessage(), e);
		}
	};
    executor.schedule(task, Date.from(resultTime.atZone(ZoneId.systemDefault()).toInstant()));
  }
}
