package com.vote.infrastructure.scheduler;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.vote.model.entity.Session;
import com.vote.service.SessionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class RunAfterStartup {

	private final VoteResultScheduler voteScheduler;
	private final SessionService sessionService;

	@EventListener(ApplicationReadyEvent.class)
	public void runAfterStartup() {

		List<Session> sessionFutureTime = sessionService.findAllByDtStartAfterNow(LocalDateTime.now());

		voteScheduler.schedulingResults(sessionFutureTime);
		log.info("Scheduling Sessions Result Ended");
	}
}
