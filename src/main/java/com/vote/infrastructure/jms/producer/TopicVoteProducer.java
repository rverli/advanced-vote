package com.vote.infrastructure.jms.producer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class TopicVoteProducer {

  @Value("${app.kafka.producer.topic.vote.name}")
  private String topicName;

  private final KafkaTemplate<String, String> kafkaTemplate;

  public void send(String message) {
    log.info("Paylod enviado: {}", message);
    kafkaTemplate.send(topicName, message);
  }
}
