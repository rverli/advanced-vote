package com.vote.infrastructure.jms.consumer;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.vote.model.dto.SessionDto;
import com.vote.model.dto.VoteDto;
import com.vote.model.exception.CpfInvalidException;
import com.vote.model.exception.SessionNotAnonymousException;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.model.exception.SessionPeriodException;
import com.vote.model.exception.UserUnableToVoteException;
import com.vote.model.exception.VoteException;
import com.vote.service.SessionService;
import com.vote.service.VoteService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class TopicVoteConsumer {

	private final VoteService voteService;
	private final SessionService sessionService;

	@Value("${topic.vote.consumer")
	private String topicName;
	
	@RetryableTopic(
            attempts = "${app.kafka.consumer.topic.retry.attempts}",
            backoff = @Backoff(
                    delayExpression = "${app.kafka.consumer.topic.retry.backoff.delay}",
                    maxDelayExpression = "${app.kafka.consumer.topic.retry.backoff.max-delay}",
                    multiplierExpression = "${app.kafka.consumer.topic.retry.backoff.multiplier}"
            ),
            replicationFactor = "${app.kafka.consumer.topic.replicas}",
            numPartitions = "${app.kafka.consumer.topic.particoes}"
    )
	@KafkaListener(topics = "${app.kafka.consumer.topic.name}")
	public void consume(@Payload final String payload) throws SessionNotFoundException, SessionPeriodException, CpfInvalidException, UserUnableToVoteException, VoteException, JSONException, SessionNotAnonymousException {

		VoteDto voteDto = new Gson().fromJson(payload, VoteDto.class);
		
		SessionDto sessionDto = sessionService.findSessionByName(voteDto.getNmSession());

		VoteDto vote = voteService.create(voteDto, sessionDto);
		log.info("Created vote id {}!", vote.getId());
	}

	@DltHandler
    public void dlt(@Payload final String mensagem,
                    @Header(KafkaHeaders.RECEIVED_TOPIC) final String topico) {
        log.error("DLT: mensagem={} para o topic={}", mensagem, topico);
    }	
}
