package com.vote.infrastructure.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "check-user-able-to-vote", url = "${http.check-cpf.base-url}")
public interface UserNoAuthClient {

  @GetMapping(value = "/users/{cpf}")
  String getUser(@PathVariable String cpf);
}
