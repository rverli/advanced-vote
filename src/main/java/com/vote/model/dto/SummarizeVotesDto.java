package com.vote.model.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SummarizeVotesDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Schema(description = "Session", required = true)
  private SessionDto session;

  @Schema(description = "Results", required = true)
  private List<CountVotes> result;
}
