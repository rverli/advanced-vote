package com.vote.model.dto;

import static com.vote.model.constants.LabelsConstants.VOTE_ID_LABEL;
import static com.vote.model.constants.LabelsConstants.VOTE_LABEL;
import static com.vote.model.constants.LabelsConstants.VOTE_SESSION_LABEL;
import static com.vote.model.constants.LabelsConstants.VOTE_USER_LABEL;
import static com.vote.model.constants.ErrorMessages.NULL_SESSION_VOTE;
import static com.vote.model.constants.ErrorMessages.NULL_VOTE;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoteDto implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Schema(description = VOTE_ID_LABEL)
  private Integer id;

  @Schema(description = VOTE_SESSION_LABEL, required = true, example = "Session obj")
  @NotNull(message = NULL_SESSION_VOTE)
  private String nmSession;

  @Schema(description = VOTE_USER_LABEL, required = true, example = "User obj")
  private UserDto user;

  @Schema(description = VOTE_LABEL, required = true, example = "Sim")
  @NotNull(message = NULL_VOTE)
  private String vote;
}
