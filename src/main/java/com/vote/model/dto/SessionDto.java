package com.vote.model.dto;

import static com.vote.model.constants.ErrorMessages.INVALID_SESSION_START_DATE;
import static com.vote.model.constants.ErrorMessages.NULL_SESSION_DATE;
import static com.vote.model.constants.ErrorMessages.NULL_SESSION_NM_SESSION;
import static com.vote.model.constants.ErrorMessages.POSITIVE_SESSION_TTL;
import static com.vote.model.constants.LabelsConstants.DATE_TIME_PATTERN;
import static com.vote.model.constants.LabelsConstants.SESSION_ANONYMOUS_LABEL;
import static com.vote.model.constants.LabelsConstants.SESSION_DT_START_LABEL;
import static com.vote.model.constants.LabelsConstants.SESSION_ID_LABEL;
import static com.vote.model.constants.LabelsConstants.SESSION_NAME_LABEL;
import static com.vote.model.constants.LabelsConstants.SESSION_TTL_LABEL;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Schema(description = SESSION_ID_LABEL)
  private Integer id;

  @Schema(description = SESSION_NAME_LABEL, required = true, example = "Session1")
  @NotEmpty(message = NULL_SESSION_NM_SESSION)
  private String nmSession;

  @Schema(description = SESSION_DT_START_LABEL, required = true, example = DATE_TIME_PATTERN)
  @NotNull(message = NULL_SESSION_DATE)
  @Future(message = INVALID_SESSION_START_DATE)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN)
  private LocalDateTime dtStart;

  @Schema(description = SESSION_TTL_LABEL, required = true, example = "60", defaultValue = "1")
  @Positive(message = POSITIVE_SESSION_TTL)
  @Builder.Default
  private int ttl = 1;

  @Schema(description = SESSION_ANONYMOUS_LABEL, example = "true")
  private boolean anonymous;
  
  private AgendaDto agenda;
}
