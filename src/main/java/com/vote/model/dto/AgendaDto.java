package com.vote.model.dto;

import static com.vote.model.constants.ErrorMessages.NULL_AGENDA_NM_SESSION;
import static com.vote.model.constants.ErrorMessages.NULL_AGENDA_SUBJECT;
import static com.vote.model.constants.LabelsConstants.AGENDA_ID_LABEL;
import static com.vote.model.constants.LabelsConstants.AGENDA_NAME_LABEL;
import static com.vote.model.constants.LabelsConstants.AGENDA_SUBJECT_LABEL;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter @Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class AgendaDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Schema(description = AGENDA_ID_LABEL)
  private Integer id;

  @Schema(description = AGENDA_NAME_LABEL, required = true, example = "Agenda1")
  @NotEmpty(message = NULL_AGENDA_NM_SESSION)
  private String nmAgenda;

  @Schema(description = AGENDA_SUBJECT_LABEL, required = true,
      example = "Session about free speech")
  @NotEmpty(message = NULL_AGENDA_SUBJECT)
  private String subject;
}
