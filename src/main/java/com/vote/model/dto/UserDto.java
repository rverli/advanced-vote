package com.vote.model.dto;

import static com.vote.model.constants.LabelsConstants.USER_CPF_LABEL;
import static com.vote.model.constants.LabelsConstants.USER_ID_LABEL;
import static com.vote.model.constants.LabelsConstants.USER_NAME_LABEL;
import static com.vote.model.constants.ErrorMessages.NULL_CPF_NAME;
import static com.vote.model.constants.ErrorMessages.NULL_USER_NAME;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Schema(description = USER_ID_LABEL)
  private Integer id;

  @Schema(description = USER_NAME_LABEL, required = true, example = "Maria das Couves")
  @NotNull(message = NULL_USER_NAME)
  private String nmUser;

  @CPF
  @Schema(description = USER_CPF_LABEL, required = true, example = "08512724799")
  @NotNull(message = NULL_CPF_NAME)
  private String cpf;
}
