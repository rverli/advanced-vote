package com.vote.model.dto;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountVotes implements Serializable {

  private static final long serialVersionUID = 1L;

  @Schema(description = "Vote", required = true)
  private String vote;

  @Schema(description = "Qtd", required = true)
  private long qtd;
}
