package com.vote.model.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LabelsConstants {

  public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

  public static final String AGENDA_ID_LABEL = "Agenda id";
  public static final String AGENDA_NAME_LABEL = "Agenda name";
  public static final String AGENDA_SUBJECT_LABEL = "Agenda subject";
  
  public static final String SESSION_ID_LABEL = "Session id";
  public static final String SESSION_NAME_LABEL = "Session name";
  public static final String SESSION_DT_START_LABEL = "Session start date";
  public static final String SESSION_TTL_LABEL = "Session TTL";
  public static final String SESSION_ANONYMOUS_LABEL = "Is a Secret Vote?";

  public static final String USER_ID_LABEL = "User id";
  public static final String USER_NAME_LABEL = "User name";
  public static final String USER_CPF_LABEL = "User CPF";

  public static final String VOTE_ID_LABEL = "Vote id";
  public static final String VOTE_SESSION_LABEL = "Vote session";
  public static final String VOTE_USER_LABEL = "Vote user";
  public static final String VOTE_LABEL = "Vote";
}
