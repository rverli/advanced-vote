package com.vote.model.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorMessages {

	public static final String NULL_AGENDA_SUBJECT = "The agenda subject must not be empty.";
	public static final String NULL_AGENDA_NM_SESSION = "The agenda name must not be empty.";
	public static final String NULL_AGENDA_ID = "The agenda id must not be empty.";

	public static final String INVALID_SESSION_START_DATE = "The Session start datetime must be a future date.";
	public static final String NULL_SESSION_DATE = "The Session start datetime must not be null.";
	public static final String NULL_SESSION_SUBJECT = "The Session subject must not be empty.";
	public static final String NULL_SESSION_NM_SESSION = "The Session name must not be empty.";
	public static final String NULL_SESSION_ID = "The session id must not be empty.";
	public static final String POSITIVE_SESSION_TTL = "The Session ttl must be positive.";

	public static final String NULL_USER_NAME = "The user name must not be null";
	public static final String NULL_CPF_NAME = "The cpf user must not be null";

	public static final String NULL_SESSION_VOTE = "The vote session must not be null";
	public static final String NULL_VOTE = "The vote must not be null";
}
