package com.vote.model.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CheckUserAbleToVoteEnum {
  ABLE_TO_VOTE(), UNABLE_TO_VOTE();
}

