package com.vote.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> {
    
    @CreationTimestamp
    @Column(name = "created_on")
    protected LocalDateTime createdOn;
    
    @CreatedBy
    @Column(name = "create_by")
    protected U createdBy;

    @LastModifiedDate
	@Column(name = "updated_on")
    protected LocalDateTime updatedOn;
    
    @LastModifiedBy
	@Column(name = "modify_by")
	protected U updatedBy;
}
