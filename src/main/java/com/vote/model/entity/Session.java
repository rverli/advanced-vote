package com.vote.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TB_SESSION")
@EntityListeners(AuditingEntityListener.class)
public class Session extends Auditable<String> implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonIgnore
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", unique = true, nullable = false)
  private Integer id;

  @Column(name = "NM_SESSION")
  private String nmSession;

  @Column(name = "DT_START")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime dtStart;

  @Column(name = "TTL")
  @Builder.Default
  private int ttl = 1;

  @Column(name = "ANONYMOUS")
  private boolean anonymous;
  
  @ManyToOne
  @JoinColumn(name = "ID_AGENDA")
  private Agenda agenda;
}
