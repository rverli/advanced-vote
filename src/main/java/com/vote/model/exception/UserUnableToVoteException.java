package com.vote.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "User is not allowed to vote!")
public class UserUnableToVoteException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserUnableToVoteException() {
		super();
	}

	public UserUnableToVoteException(String errorMessage) {
		super(errorMessage);
	}

	public UserUnableToVoteException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
