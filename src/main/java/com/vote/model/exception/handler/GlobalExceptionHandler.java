package com.vote.model.exception.handler;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.vote.model.dto.response.ErrorResponse;
import com.vote.model.exception.CpfInvalidException;
import com.vote.model.exception.SessionNotAnonymousException;
import com.vote.model.exception.SessionNotFoundException;
import com.vote.model.exception.SessionPeriodException;
import com.vote.model.exception.UserNotFoundException;
import com.vote.model.exception.UserUnableToVoteException;
import com.vote.model.exception.VoteException;

@RestControllerAdvice
public class GlobalExceptionHandler {
 
  @ExceptionHandler({CpfInvalidException.class})
  public final ResponseEntity<ErrorResponse> handleCpfInvalidException(
      HttpServletRequest request, CpfInvalidException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
  
  @ExceptionHandler({SessionNotAnonymousException.class})
  public final ResponseEntity<ErrorResponse> handleSessionNotAnonymousException(
      HttpServletRequest request, SessionNotAnonymousException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
  
  @ExceptionHandler({SessionNotFoundException.class})
  public final ResponseEntity<ErrorResponse> handleSessionNotFoundException(
      HttpServletRequest request, SessionNotFoundException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
  
  @ExceptionHandler({SessionPeriodException.class})
  public final ResponseEntity<ErrorResponse> handleSessionPeriodException(
      HttpServletRequest request, SessionPeriodException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
  
  @ExceptionHandler({UserNotFoundException.class})
  public final ResponseEntity<ErrorResponse> handleUserNotFoundException(
      HttpServletRequest request, UserNotFoundException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
  
  @ExceptionHandler({UserUnableToVoteException.class})
  public final ResponseEntity<ErrorResponse> handleUserUnableToVoteException(
      HttpServletRequest request, UserUnableToVoteException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
  
  @ExceptionHandler({VoteException.class})
  public final ResponseEntity<ErrorResponse> handleVoteException(
      HttpServletRequest request, VoteException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            ErrorResponse.builder()
                .detail(ex.getMessage())
                .title(ex.getClass().getSimpleName())
                .status(HttpStatus.BAD_REQUEST.value())
                .instance(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build());
  }
}
