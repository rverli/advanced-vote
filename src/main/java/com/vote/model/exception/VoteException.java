package com.vote.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class VoteException extends Exception {

	private static final long serialVersionUID = 1L;

	public VoteException() {
		super();
	}

	public VoteException(String errorMessage) {
		super(errorMessage);
	}

	public VoteException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
