package com.vote.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Session is not anonymous! Please send the user")
public class SessionNotAnonymousException extends Exception {

	private static final long serialVersionUID = 1L;

	public SessionNotAnonymousException() {
		super();
	}

	public SessionNotAnonymousException(String errorMessage) {
		super(errorMessage);
	}

	public SessionNotAnonymousException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
