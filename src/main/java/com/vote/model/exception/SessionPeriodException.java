package com.vote.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Vote not allowed, Session is not open!")
public class SessionPeriodException extends Exception {

	private static final long serialVersionUID = 1L;

	public SessionPeriodException() {
		super();
	}

	public SessionPeriodException(String errorMessage) {
		super(errorMessage);
	}

	public SessionPeriodException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
