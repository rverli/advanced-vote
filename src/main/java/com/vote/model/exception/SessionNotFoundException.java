package com.vote.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Session NotFound")
public class SessionNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public SessionNotFoundException() {
		super();
	}

	public SessionNotFoundException(String errorMessage) {
		super(errorMessage);
	}

	public SessionNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
