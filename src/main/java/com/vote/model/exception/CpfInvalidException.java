package com.vote.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid CPF!")
public class CpfInvalidException extends Exception {

	private static final long serialVersionUID = 1L;

	public CpfInvalidException() {
		super();
	}

	public CpfInvalidException(String errorMessage) {
		super(errorMessage);
	}

	public CpfInvalidException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
