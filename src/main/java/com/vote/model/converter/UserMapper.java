package com.vote.model.converter;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.vote.model.dto.UserDto;
import com.vote.model.entity.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

    User toEntity(UserDto dto);
    UserDto toDto(User entity);
}
