package com.vote.model.converter;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.vote.model.dto.AgendaDto;
import com.vote.model.entity.Agenda;

@Mapper(componentModel = "spring")
public interface AgendaMapper {

    AgendaMapper INSTANCE = Mappers.getMapper( AgendaMapper.class );

    Agenda toEntity(AgendaDto dto);
    AgendaDto toDto(Agenda entity);
}
