package com.vote.model.converter;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.vote.model.dto.SessionDto;
import com.vote.model.entity.Session;

@Mapper(componentModel = "spring")
public interface SessionMapper {

    SessionMapper INSTANCE = Mappers.getMapper( SessionMapper.class );

    Session toEntity(SessionDto dto);
    SessionDto toDto(Session entity);
}
