package com.vote.model.converter;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.vote.model.dto.VoteDto;
import com.vote.model.entity.Vote;

@Mapper(componentModel = "spring")
public interface VoteMapper {

    VoteMapper INSTANCE = Mappers.getMapper( VoteMapper.class );

    Vote toEntity(VoteDto dto);
    VoteDto toDto(Vote entity);
}
