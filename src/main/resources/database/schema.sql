CREATE TABLE IF NOT EXISTS TB_AGENDA (

	ID  SERIAL PRIMARY KEY,
	NM_AGENDA varchar(100) not null,
	SUBJECT varchar(100) not null,
	created_on TIMESTAMP DEFAULT now(),
	create_by VARCHAR(20) DEFAULT user,
	updated_on TIMESTAMP NULL,
	modify_by VARCHAR(20) NULL
);

CREATE TABLE IF NOT EXISTS TB_SESSION (

	ID  SERIAL PRIMARY KEY,
	NM_SESSION varchar(100) UNIQUE not null,
	DT_START timestamp not null,
	TTL int default 1 not null,
	ANONYMOUS boolean default false not null,
	ID_AGENDA INT NOT NULL,
	created_on TIMESTAMP DEFAULT now(),
	create_by VARCHAR(20) DEFAULT user,
	updated_on TIMESTAMP NULL,
	modify_by VARCHAR(20) NULL,
	CONSTRAINT AGENDA_fk
      FOREIGN KEY(ID_AGENDA)
	  REFERENCES TB_AGENDA(ID)
);

CREATE TABLE IF NOT EXISTS TB_USER (

	ID  SERIAL PRIMARY KEY,
	NM_USER varchar(100) not null,
	CPF varchar(100) UNIQUE not null,
	created_on TIMESTAMP DEFAULT now(),
	create_by VARCHAR(20) DEFAULT user,
	updated_on TIMESTAMP NULL,
	modify_by VARCHAR(20) NULL
);

CREATE TABLE IF NOT EXISTS TB_VOTE (

	ID  SERIAL PRIMARY KEY,
	ID_SESSION int not null,
	ID_USER int,
	VOTE varchar(3) not null,
	created_on TIMESTAMP DEFAULT now(),
	create_by VARCHAR(20) DEFAULT user,
	updated_on TIMESTAMP NULL,
	modify_by VARCHAR(20) NULL,
    CONSTRAINT SESSION_fk
      FOREIGN KEY(ID_SESSION)
	  REFERENCES TB_SESSION(ID),
    CONSTRAINT USER_fk
      FOREIGN KEY(ID_USER)
	  REFERENCES TB_USER(ID)
);