# Advanced-Vote System

Sistema para realizar e computar votos de acordo com documentação fornecida.

## Construido com:

* [GitLab](https://gitlab.com/)
* [Java](https://www.java.com/en/)
* [Apache Kafka](https://kafka.apache.org/)
* [Postgresql](https://www.postgresql.org/)
* [Spring Data](https://spring.io/projects/spring-data)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Feignclient](https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html)
* [Swagger API Documentation](https://swagger.io/)
* [Apache Maven](https://maven.apache.org/)

### Pre-requisitos

O que você precisa para utilizar o software

```
Apache Kafka
Gitlab Account
JAVA 17
```

## Iniciando

As instruções a seguir irão guiar na utilização do software

## Clonando o código fonte

Para prosseguir com a configuração do software será necessário realizar o clone do repositório de código fonte no GitHub. \

[Clonar Repositório](https://gitlab.com/rverli/advanced-vote)

## Configuração Apache Kafka

Com pré-requisito ter uma instância do Apache Kafka instalada e rodando, no segundo passo utilizaremos a URL de conexão no projeto.\

No arquivo application.properties, localizado em src/main/resources substitua o valor dos seguintes campos:\
spring.kafka.producer.bootstrap-servers\
spring.kafka.consumer.bootstrap-servers\

## Banco de Dados

O sistema já está utilizando uma instância de banco de dados postgres válida, se preferir utilizar sua própria instância, poderá faze-la substituindo os campos relativos a url e credenciais no mesmo arquivo da configuração do Kafka.\

O script com a criação das tabelas e indexes encontra-se em src/main/resources/database/schema.sql\

## Rodando a aplicação

A execução da aplicação via terminal será da seguinte forma: \

1- Navegar até o diretório da aplicação que foi clonado de acordo com as instruções;\
2- Executar os seguintes comandos Maven:\
    - mvn clean package\
    - mvn spring-boot:run\

## Documentação da API

O sistema foi documentado a nível de API utilizando o Swagger e encontra-se disponível no seguinte endereço:\

http://<HOST>:<PORT>/swagger-ui.html\

## Pontos de Melhoria

#1- Implamentar segurança (JWT ou OAuth2);\
#2- Inicialmente desenvolvido com Spring Boot 3, porém, devido a um bug com Swagger na versão 3, se fez necessário downgrade;\
#3- Tratar as DLT/DLQ;\
#4- Armazenar variaveis e senhas em local apropriado(Vault, Consul ou outro);\
#5- Dependendo do requisito, a utilização do Spring Webflux traria performance, por ser não bloqueante.\

## Tarefas Bônus

#TAREFA BONUS 1 - API de validação de CPF que consta nas instruções não está disponível. Está implementado, mas não utiliza.\
#TAREFA BONUS 2 - Utilizando Kafka para mensageria\
#TAREFA BONUS 3 - Não consta nas instruções se é necessário validar o voto e retornar msg de erro para o usuário.(Implementado desta forma)\
	Caso, o usuário não necessitar da resposta, a API ficario muito mais performance\
#TAREFA BONUS 4 - Foi utilizado o versionamento via path(v1, v2 e etc) por ser mais dev-friendly e facilitar em navegar entre as versões.\

## Author

* **Renan Verli** 

